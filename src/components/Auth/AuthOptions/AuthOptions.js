import React from "react";
import { Button } from "semantic-ui-react";

import "./AuthOptions.scss";

const AuthOptions = ({ setSelectedForm }) => {
  return (
    <div className="auth-options">
      <h2>Milhões de músicas gratuitas no Musicfy</h2>
      <Button className="register" onClick={() => setSelectedForm("register")}>
        Registre-se de graça
      </Button>
      <Button className="login" onClick={() => setSelectedForm("login")}>
        Fazer Login
      </Button>
    </div>
  );
};

export default AuthOptions;
