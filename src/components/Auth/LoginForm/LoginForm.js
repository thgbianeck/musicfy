import React, { useState } from "react";
import { Button, Icon, Form, Input } from "semantic-ui-react";
import { toast } from "react-toastify";
import { validateEmail } from "../../../utils/Validations";
import firebase from "../../../utils/Firebase";
import "firebase/auth";

import "./LoginForm.scss";

const LoginForm = ({ setSelectedForm }) => {
  const [showPassword, setShowPassword] = useState(false);
  const [formData, setFormData] = useState(defaultValueForm());
  const [formError, setFormError] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [userActive, setUserActive] = useState(true);
  const [user, setUser] = useState(null);

  const handlerShowPassword = () => setShowPassword(!showPassword);

  const onChange = (e) => {
    let { name, value } = e.target;

    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const onSubmit = () => {
    let { email, password } = formData;
    setFormError({});
    let errors = {};
    let formOk = true;

    if (!validateEmail(email)) {
      errors.email = true;
      formOk = false;
    }
    if (password.length < 6) {
      errors.password = true;
      formOk = false;
    }

    setFormError(errors);

    if (formOk) {
      setIsLoading(true);
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then((response) => {
          setUser(response.user);
          setUserActive(response.user.emailVerified);
          if (!response.user.emailVerified) {
            toast.warning(
              "É necessário verificar sua conta antes de fazer login."
            );
          }
        })
        .catch((err) => {
          console.log(err);
          handlerErrors(err.code);
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  };

  return (
    <div className="login-form">
      <h1>Música para todos</h1>
      <Form onSubmit={onSubmit}>
        <Form.Field>
          <Input
            type="text"
            name="email"
            placeholder="E-mail"
            icon="mail outline"
            onChange={onChange}
            error={formError.email}
          />
          {formError.email && (
            <span className="error-text">
              Por favor, digite um email válido.
            </span>
          )}
        </Form.Field>
        <Form.Field>
          <Input
            type={showPassword ? "text" : "password"}
            name="password"
            placeholder="Senha"
            icon={
              <Icon
                name={showPassword ? "eye slash outline" : "eye"}
                link
                onClick={handlerShowPassword}
              />
            }
            onChange={onChange}
            error={formError.password}
          />
          {formError.password && (
            <span className="error-text">
              Por favor, digite uma senha com mais de 5 caracteres.
            </span>
          )}
        </Form.Field>
        <Button type="submit" loading={isLoading}>
          Iniciar Sessão
        </Button>
      </Form>
      {!userActive && (
        <ButtonResetSendEmailVerification
          user={user}
          setIsLoading={setIsLoading}
          setUserActive={setUserActive}
        />
      )}
      <div className="login-form__options">
        <p onClick={() => setSelectedForm(null)}>Voltar</p>
        <p>
          Não tem conta?{" "}
          <span onClick={() => setSelectedForm("register")}>Registre-se</span>
        </p>
      </div>
    </div>
  );
};

const ButtonResetSendEmailVerification = ({
  user,
  setIsLoading,
  setUserActive,
}) => {
  const resendVerificationEmail = () => {
    user
      .sendEmailVerification()
      .then(() => {
        toast.success("O E-mail de verificação foi enviado");
      })
      .catch((err) => {
        handlerErrors(err.code);
      })
      .finally(() => {
        setIsLoading(false);
        setUserActive(true);
      });
  };

  return (
    <div className="resend-verification-email">
      <p>
        Se não recebeu o e-mail de verificação, pode voltar a envia-lo clicando{" "}
        <span onClick={resendVerificationEmail}>aqui.</span>
      </p>
    </div>
  );
};

const handlerErrors = (code) => {
  switch (code) {
    case "auth/wrong-password":
      toast.warning("A senha é inválida ou o usuário não possui uma senha.");
      break;
    case "auth/too-many-requests":
      toast.warning(
        "Você enviou muitas solicitações de envio de e-mail em pouco tempo"
      );
      break;
    case "auth/user-not-found":
      toast.warning(
        "Não há registro de usuário correspondente a este. O usuário pode ter sido excluído."
      );
      break;

    default:
      break;
  }
};

const defaultValueForm = () => {
  return {
    email: "",
    password: "",
  };
};

export default LoginForm;
