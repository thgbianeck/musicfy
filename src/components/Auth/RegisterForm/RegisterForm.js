import React, { useState } from "react";
import { Button, Icon, Form, Input } from "semantic-ui-react";
import { toast } from "react-toastify";
import firebase from "../../../utils/Firebase";
import { validateEmail } from "../../../utils/Validations";
import "firebase/auth";

import "./RegisterForm.scss";

const RegisterForm = ({ setSelectedForm }) => {
  const [formData, setFormData] = useState(defaultValueForm());
  const [showPassword, setShowPassword] = useState(false);
  const [formError, setFormError] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  const handlerShowPassword = () => setShowPassword(!showPassword);

  const onChange = (e) => {
    let { name, value } = e.target;

    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const onSubmit = () => {
    let { email, password, username } = formData;
    setFormError({});
    let errors = {};
    let formOk = true;

    if (!validateEmail(email)) {
      errors.email = true;
      formOk = false;
    }
    if (password.length < 6) {
      errors.password = true;
      formOk = false;
    }

    if (!username) {
      errors.username = true;
      formOk = false;
    }

    setFormError(errors);

    if (formOk) {
      setIsLoading(true);
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          changeUserName();
          sendVerificationEmail();
        })
        .catch(() => {
          toast.error("Erro ao criar a conta");
        })
        .finally(() => {
          setIsLoading(false);
          setSelectedForm(null);
        });
    }
  };

  const changeUserName = () => {
    firebase
      .auth()
      .currentUser.updateProfile({
        displayName: formData.username,
      })
      .catch(() => {
        toast.error("Erro ao atribuir nome ao usuário.");
      });
  };

  const sendVerificationEmail = () => {
    firebase
      .auth()
      .currentUser.sendEmailVerification()
      .then(() => {
        toast.success("Foi enviado um e-mail de verificação");
      })
      .catch(() => {
        toast.error("Erro ao enviar o e-mail de verificação.");
      });
  };

  return (
    <div className="register-form">
      <h1>Comece agora com uma conta gratuita no Musicfy</h1>
      <Form onSubmit={onSubmit} onChange={onChange}>
        <Form.Field>
          <Input
            type="text"
            name="email"
            placeholder="E-mail"
            icon="mail outline"
            onChange={onChange}
            error={formError.email}
          />
          {formError.email && (
            <span className="error-text">
              Por favor, digite um email válido.
            </span>
          )}
        </Form.Field>
        <Form.Field>
          <Input
            type={showPassword ? "text" : "password"}
            name="password"
            placeholder="Senha"
            icon={
              <Icon
                name={showPassword ? "eye slash outline" : "eye"}
                link
                onClick={handlerShowPassword}
              />
            }
            onChange={onChange}
            error={formError.password}
          />
          {formError.password && (
            <span className="error-text">
              Por favor, digite uma senha com mais de 5 caracteres.
            </span>
          )}
        </Form.Field>
        <Form.Field>
          <Input
            type="text"
            name="username"
            placeholder="Como deveríamos te chamar"
            icon="user circle outline"
            onChange={onChange}
            error={formError.username}
          />
          {formError.username && (
            <span className="error-text">
              Por favor, digite um nome de usuário válido.
            </span>
          )}
          <Button type="submit" loading={isLoading}>
            Continuar
          </Button>
        </Form.Field>
      </Form>
      <div className="register-form__options">
        <p onClick={() => setSelectedForm(null)}>Voltar</p>
        <p>
          Já tem conta no Musicfy?{" "}
          <span onClick={() => setSelectedForm("login")}>Iniciar sessão</span>
        </p>
      </div>
    </div>
  );
};

const defaultValueForm = () => {
  return {
    email: "",
    password: "",
    username: "",
  };
};

export default RegisterForm;
